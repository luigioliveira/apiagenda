const express = require('express');
const datetime = require("node-datetime");
const bodyParser = require('body-parser');
const db = require('./db/connection');
const port = 8000;
const server = "Local/UNICSUL";
const dt = datetime.create();
let formattedDate = dt.format('d/m/Y H:M:S');


let app = express();
app.use(bodyParser.json());

app.get('/contatos', (req, res) => {
    let cmd_selectAll = 'SELECT * FROM CONTATO';
    db.query(cmd_selectAll, (error, rows) => {

        // Retorna exatamente o que está vindo da query
        res.status(200).json(rows);


        // Utiliza o map para cada linha e então retorna o que deseja
        // res.json(rows.map((row)=>{
        //     return {nome: row.nome}
        // }))
    })
})

app.post('/contatos', (req, res) => {
    let body = req.body;
    let cmd_insert = `INSERT INTO CONTATO (nome, idade, email, numero) VALUES (?,?,?,?)`
    db.query(cmd_insert, [body.nome, body.idade, body.email, body.numero], (error,
        results) => {
            if(error){
                return res.status(400).json({ error: error });
            } 
            else {
                console.log("==============================================================");
                console.log("A new register was inserted on Contato table.");
                console.log(`ID: ${results.insertId} | AffectedRows: ${results.affectedRows}`);
                console.log(`NomeField: ${body.nome}  | IdadeField: ${body.idade}`);
                console.log(`EmailField: ${body.email} | NumeroField: ${body.numero}`);
                console.log("==============================================================");
                return res.status(201).json({message: "Registro inserido com sucesso!", result: results})
            }
    });
})

app.delete('/contatos', (req, res) => {
    let body = req.body;
    let cmd_delete = `DELETE FROM CONTATO WHERE ID = ?`
    db.query(cmd_delete, [body.id], (error,
        results) => {
            if(error){
                return res.status(400).json({ error: error });
            } 
            else {
                console.log("==============================================================");
                console.log(`A register that has the ID ${body.id} was deleted on Contato table .`);
                console.log(`AffectedRows: ${results.affectedRows}`);
                console.log("==============================================================");
                return res.status(201).json({message: "Registro deletado com sucesso!", result: results})
            }
    });
})

app.patch('/contatos', (req, res) => {
    let body = req.body;
    let cmd_update = `UPDATE CONTATO SET ? WHERE ID = ?`

    // db.query(cmd_update, [{[body.field]: body.new_value, [body.field2]: body.new_value2}, body.id], (error,
    //     results) => {
    //         if(error){
    //             return res.status(400).json({ error: error });
    //         } 
    //         else {
    //             console.log("==============================================================");
    //             console.log(`A register that has the ID ${body.id} was deleted on Contato table .`);
    //             console.log(`AffectedRows: ${results.affectedRows}`);
    //             console.log("==============================================================");
    //             return res.status(201).json({message: "Registro deletado com sucesso!", result: results})
    //         }
    // });

    db.query(cmd_update, [body.data, body.id], (error,
        results) => {
            if(error){
                return res.status(400).json({ error: error });
            } 
            else {
                console.log("==============================================================");
                console.log(`A register that has the ID ${body.id} was deleted on Contato table .`);
                console.log(`AffectedRows: ${results.affectedRows}`);
                console.log("==============================================================");
                return res.status(201).json({message: "Registro deletado com sucesso!", result: results})
            }
    });
})



app.listen(port, () => {
    console.log("==============================================================");
    console.log("Projeto API Agenda por Luigi Oliveira iniciado!");
    console.log("Projeto executando na porta: " + port);
    console.log("Servidor: " + server + " | Horário: " + formattedDate);
    console.log("==============================================================");
});