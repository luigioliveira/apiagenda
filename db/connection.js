const mysql = require('mysql2');
const datetime = require("node-datetime");
const dt = datetime.create();
let formattedDate = dt.format('d/m/Y H:M:S');
const server = "localhost";

const connection = mysql.createConnection({
    host: server,
    user: 'root',
    password: 'root',
    database: 'agenda'
});

connection.connect((error)=>{
    if (error){
        console.log(error);
    }
    else {
        console.log("==============================================================");
        console.log("Banco de dados conectado com sucesso!");
        console.log("Servidor: " + server + " | Horário: " + formattedDate);
        console.log("==============================================================");
    }
});

module.exports = connection;